﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class CamController : MonoBehaviour
{
    // Private vars
    private int camCurrentHeightIndex;
    private int mCurrentIndex;
    private string[] res;
    [SerializeField] private Vector3[] camSteps;
    private Vector3 newPos;
    private Vector3 terrainSize;
    private Vector3 terrainCenter;
    [SerializeField] private Component selectionManager;
    
    // Public vars
    public float camNormalSpeed;
    public float camMoveSpeed;
    public float camMoveTime;
    public float camRotationSpeed;
    public float switchSpeed;
    public float screenBorderThickness;
    public Camera sceneCamera;

    // Start is called before the first frame update
    private void Start() {
        camCurrentHeightIndex = 1;
        sceneCamera = Camera.main;
        sceneCamera.enabled = true;
        camSteps = new Vector3[3];
        newPos.y = 0;
        newPos = GameObject.Find("Vehicle_caravan/Caravan_Body").GetComponent<MeshRenderer>().bounds.center; // Places the camera on the caravan when scene starts
        terrainSize = GameObject.Find("Terrain").GetComponent<Terrain>().terrainData.size;
        terrainCenter = new Vector3(GameObject.Find("Terrain").GetComponent<TerrainCollider>().bounds.center.x,GameObject.Find("Terrain").GetComponent<TerrainCollider>().bounds.center.y,GameObject.Find("Terrain").GetComponent<TerrainCollider>().bounds.center.z);
        Debug.Log("\n Debug tools : \n - Press F1 to display cameras position \n - Press F2 to display terrain dimensions \n - Press F3 to display terrain centers position \n - Press F4 to display terrain limits position \n - Press F5 to display caravan position"); // Details debug commands in console at start
        selectionManager = (Component)GameObject.FindObjectOfType(typeof(SelectionManager));
    }
    
    // Update is called once per frame
    private void Update() {
        #if UNITY_EDITOR // Allows the script to call Unity Editor from the game code for build purposes
        res = UnityEditor.UnityStats.screenRes.Split('x'); // Gets width and height of the Game window from its Stats popup
        #endif
        
        RefreshStepsPos();
        InputHandler();
        DebugHandler();
    }

    // Refreshes steps positions to keep the camera synchronized when switching between heights while moving and/or rotating
    private void RefreshStepsPos() {
        camSteps[0] = this.transform.GetChild(0).transform.position;
        camSteps[1] = this.transform.GetChild(1).transform.position;
        camSteps[2] = this.transform.GetChild(2).transform.position;
    }

    // Input behaviors
    private void InputHandler() 
    {
        // Stops camera movement if the cursor is out of the Game window
        if (Input.mousePosition.x > int.Parse(res[0]) || Input.mousePosition.y > int.Parse(res[1]) || Input.mousePosition.x < 0 || Input.mousePosition.y < 0) 
        {
            this.camMoveSpeed = 0;
            this.camNormalSpeed = 0;
        }
        // Allows camera movement as long as the cursor is within the Game window
        if (Input.mousePosition.x < int.Parse(res[0]) && Input.mousePosition.y < int.Parse(res[1]) && Input.mousePosition.x > 0 && Input.mousePosition.y > 0) 
        {
            this.camMoveSpeed = 10;
            this.camNormalSpeed = 2;

            // Changes camera speed if LeftShift is held
            if (Input.GetKey(KeyCode.LeftShift)) 
            {
                camMoveSpeed = camNormalSpeed * 2.5f;
            } else {
                camMoveSpeed = camNormalSpeed;
            }
            // Changes camera speed if LeftShift is held and mouse is on the edge of the screen
            if (Input.GetKey(KeyCode.LeftShift) && (Input.mousePosition.y >= Screen.height - screenBorderThickness || Input.mousePosition.y <= screenBorderThickness || Input.mousePosition.x >= Screen.width - screenBorderThickness || Input.mousePosition.x <= screenBorderThickness)) 
            {
                camMoveSpeed = camNormalSpeed * 3.5f;
            }
            // Changes camera speed if ZQSD is held and mouse is on the edge of the screen
            if ((Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.Q)) && (Input.mousePosition.y >= Screen.height - screenBorderThickness || Input.mousePosition.y <= screenBorderThickness || Input.mousePosition.x >= Screen.width - screenBorderThickness || Input.mousePosition.x <= screenBorderThickness)) 
            {
                camMoveSpeed = camNormalSpeed * 3.5f;
            }
            // Changes camera speed if ZQSD and LeftShift are held and mouse is on the edge of the screen
            if (Input.GetKey(KeyCode.LeftShift) && (Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.Q)) && (Input.mousePosition.y >= Screen.height - screenBorderThickness || Input.mousePosition.y <= screenBorderThickness || Input.mousePosition.x >= Screen.width - screenBorderThickness || Input.mousePosition.x <= screenBorderThickness)) 
            {
                camMoveSpeed = camNormalSpeed * 5f;
            }
            // Rotate camera with A/E/MouseWheel
            if (Input.GetKey(KeyCode.A)) 
            {
                transform.Rotate(0f,camRotationSpeed,0f);
            }
            if (Input.GetKey(KeyCode.E)) 
            {
                transform.Rotate(0f,-camRotationSpeed,0f);
            }
            if (Input.GetKey(KeyCode.Mouse2) && (Input.mousePosition.x > Screen.width/2)) 
            {
                transform.Rotate(0f,camRotationSpeed,0f);
            }
            if (Input.GetKey(KeyCode.Mouse2) && (Input.mousePosition.x < Screen.width/2)) 
            {
                transform.Rotate(0f,-camRotationSpeed,0f); 
            }
            // ZQSD and Arrows movements
            if (Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.UpArrow) || Input.mousePosition.y >= Screen.height - screenBorderThickness) 
            {
                newPos += (transform.forward * camMoveSpeed);
            } 
            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow) || Input.mousePosition.y <= screenBorderThickness) 
            {
                newPos += (transform.forward * -camMoveSpeed);
            }
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow) || Input.mousePosition.x >= Screen.width - screenBorderThickness) 
            {
                newPos += (transform.right * camMoveSpeed);
            }
            if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow) || Input.mousePosition.x <= screenBorderThickness) 
            {
                newPos += (transform.right * -camMoveSpeed);
            }
            // Changes camera height position (low, middle, high) when pressing C
            if (Input.GetKeyDown(KeyCode.C)) 
            {
                camCurrentHeightIndex++;
                camCurrentHeightIndex %= 3;
            }

            this.transform.position = Vector3.Lerp(this.transform.position, newPos, Time.deltaTime * camMoveTime); // Interpolation to smooth camera basic 2D plane movements (forward, backward, sides)
            this.gameObject.transform.GetChild(3).transform.position = Vector3.Lerp(this.gameObject.transform.GetChild(3).transform.position, camSteps[camCurrentHeightIndex], switchSpeed * Time.deltaTime); // Interpolation to smooth camera height transitions (low, middle, high)
            // Limits camera movements to the edges of the terrain
            newPos.x = Mathf.Clamp(newPos.x, (terrainCenter.x + (-terrainSize.x/2)), ((terrainCenter.x + (-terrainSize.x/2) + terrainSize.x)));
            newPos.z = Mathf.Clamp(newPos.z, (terrainCenter.z + (-terrainSize.z/2)), ((terrainCenter.z + (-terrainSize.z/2) + terrainSize.z)));
        }
    }

    // Camera moves to position above town when player presses Enter Town
    public void TownView()
    {
        // this.transform.GetChild(3).transform.position = new Vector3(selectionManager.GetComponent<SelectionManager>().currentSelection.transform.position.x, selectionManager.GetComponent<SelectionManager>().currentSelection.transform.position.y + 20, selectionManager.GetComponent<SelectionManager>().currentSelection.gameObject.transform.position.z + 20);
    }

    // Debug method
    private void DebugHandler() {
        // Cameras and rig positions debug when pressing F1
        if (Input.GetKeyDown(KeyCode.F1)) {
            Debug.Log("\n" + this.transform.GetChild(0).name + " : " + "PosX" + " " + Mathf.Round(this.transform.GetChild(0).gameObject.transform.position.x) + " " + "PosY" + " " + Mathf.Round(this.transform.GetChild(0).gameObject.transform.position.y) + " " + "PosZ" + " " + Mathf.Round(this.transform.GetChild(0).gameObject.transform.position.z) +
                      "\n" + this.transform.GetChild(1).name + " : " + "PosX" + " " + Mathf.Round(this.transform.GetChild(1).gameObject.transform.position.x) + " " + "PosY" + " " + Mathf.Round(this.transform.GetChild(1).gameObject.transform.position.y) + " " + "PosZ" + " " + Mathf.Round(this.transform.GetChild(1).gameObject.transform.position.z) +
                      "\n" + this.transform.GetChild(2).name + " : " + "PosX" + " " + Mathf.Round(this.transform.GetChild(2).gameObject.transform.position.x) + " " + "PosY" + " " + Mathf.Round(this.transform.GetChild(2).gameObject.transform.position.y) + " " + "PosZ" + " " + Mathf.Round(this.transform.GetChild(2).gameObject.transform.position.z) + 
                      "\n" + this.name + " - " + "PosX" + " " + Mathf.Round(this.transform.position.x) + " " + "PosY" + " " + Mathf.Round(this.transform.position.y) + " " + "PosZ" + " " + Mathf.Round(this.transform.position.z) + "\n");
        }
        // Terrain dimensions debug when pressing F2
        if (Input.GetKeyDown(KeyCode.F2)) {    
            Debug.Log("\n" + ("Terrain X Size : " + terrainSize.x) + 
                      "\n" + ("Terrain Z Size : " + terrainSize.z) + "\n");
        }
        // Terrain centers position debug when pressing F3
        if (Input.GetKeyDown(KeyCode.F3)) {
            Debug.Log("\n" + "Terrain Center X" + " " + terrainCenter.x +
                      "\n" + "Terrain Center Z" + " " + terrainCenter.z + "\n");
        }
        // Terrain lower and upper limits debug when pressing F4
        if (Input.GetKeyDown(KeyCode.F4)) {
        Debug.Log("\n" + "Lower X Limit" + (terrainCenter.x + (-terrainSize.x/2)) + " " + "Upper X Limit" + " " + ((terrainCenter.x + (-terrainSize.x/2) + terrainSize.x)) + 
                  "\n" + "Lower Z Limit" + (terrainCenter.z + (-terrainSize.z/2)) + " " + "Upper Z Limit" + " " + ((terrainCenter.z + (-terrainSize.z/2) + terrainSize.z)) + "\n");
        }
    }
}