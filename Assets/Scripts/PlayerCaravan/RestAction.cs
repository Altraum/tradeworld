﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RestAction : MonoBehaviour
{
    // Public vars
    [SerializeField] private Component caravanControllerScript;

    private void Awake()
    {
        caravanControllerScript = (Component)GameObject.FindObjectOfType(typeof(CaravanController));
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && EventSystem.current.IsPointerOverGameObject(-1) && EventSystem.current.currentSelectedGameObject == this.gameObject)
        {   
            caravanControllerScript.GetComponent<CaravanController>().stamina = 100;
            EventSystem.current.SetSelectedGameObject(null);
        }
    }
}
