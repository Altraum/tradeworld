﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(LineRenderer))]
public class DebugPath : MonoBehaviour
{
    // Private vars
    private NavMeshAgent playerAgent;
    private LineRenderer navMeshRenderer;
    private int y;

    // Start is called before the first frame update
    void Start()
    {
        playerAgent = GameObject.Find("Player_caravan").GetComponent<NavMeshAgent>();
        navMeshRenderer = GetComponent<LineRenderer>();
        navMeshRenderer.startWidth = 0.5f;
        navMeshRenderer.endWidth = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerAgent.hasPath)
        {
            navMeshRenderer.positionCount = playerAgent.path.corners.Length;
            navMeshRenderer.SetPositions(playerAgent.path.corners);
            navMeshRenderer.enabled = true;
        }
    }
}
