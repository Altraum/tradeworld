﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(PlayerMotor))]
public class CaravanController : MonoBehaviour
{
    // Private vars
    private Camera cam;
    private Vector3 hitPosT;
    [SerializeField] private Slider staminaBar;
    private PlayerMotor motor;
    [SerializeField] private GameObject startPoint;
    private GameObject endPoint;
    private GameObject[] dummiesArray;
    private LineRenderer lineRenderer;
    private NavMeshAgent myAgent;
    private int x;
    [SerializeField] private float distance;
    private float counter;
    private float lineDist;

    // Public vars
    public LayerMask movementMask;
    public float stamina;
    public float staminaOT;
    public float lineDrawSpeed;
    public bool isMoving;
    public string uiScene;

    private void Awake()
    {
        motor = GetComponent<PlayerMotor>();
        myAgent = GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        staminaBar = (Slider)GameObject.FindObjectOfType(typeof(Slider));

        cam = Camera.main;

        staminaBar.maxValue = stamina;
        isMoving = false;
        distance = 0;

        dummiesArray = new GameObject[4];
        x = 0;

        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.startWidth = 0.5f;
        lineRenderer.endWidth = 0.5f;

        UpdateUI();
    }

    private void CalculateValues()
    {
        if (distance > 0.8f && isMoving == true && GetComponent<NavMeshAgent>().speed != 0)
        {
            stamina -= staminaOT * Time.deltaTime;
        }
        UpdateUI();
    }

    private void UpdateUI()
    {
        stamina = Mathf.Clamp(stamina, 0, 100f);
        staminaBar.value = stamina;
    }

    private void Update()
    {
        CalculateValues();

        GameObject.Find("Player_caravan").transform.position = new Vector3(GameObject.Find("Player_caravan").transform.position.x, GameObject.Find("Vehicle_caravan").transform.position.y, GameObject.Find("Player_caravan").transform.position.z);
        GameObject.Find("Vehicle_caravan").transform.position = new Vector3(GameObject.Find("Player_caravan").transform.position.x, GameObject.Find("Vehicle_caravan").transform.position.y, GameObject.Find("Player_caravan").transform.position.z);
        Vector3 relativePos = (hitPosT + new Vector3(0, .5f, 0)) - GameObject.Find("Vehicle_caravan").transform.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos);

        Quaternion current = GameObject.Find("Vehicle_caravan").transform.localRotation;

        GameObject.Find("Vehicle_caravan").transform.localRotation = Quaternion.Slerp(current, rotation, 5 * Time.deltaTime);
        GameObject.Find("Vehicle_caravan").transform.Translate(0, 0, 3 * Time.deltaTime);
        
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            if (x == 0)
            {
                Destroy(dummiesArray[x+2]);
                Destroy(dummiesArray[x+3]);
            }

            dummiesArray[x] = Instantiate(GameObject.Find("PosTestPoint"), new Vector3(GameObject.Find("Vehicle_caravan").transform.position.x, hitPosT.y, GameObject.Find("Vehicle_caravan").transform.position.z), Camera.main.transform.rotation);
            dummiesArray[x].GetComponent<MeshRenderer>().material.color = Color.red;
            dummiesArray[x].name = "PosTestPointStart";
            SceneManager.MoveGameObjectToScene(dummiesArray[x], SceneManager.GetSceneByName(uiScene));
            startPoint = dummiesArray[x];
            lineRenderer.SetPosition(0, startPoint.transform.position);
            x++;

            isMoving = true;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, movementMask))
            {
                motor.MoveToPoint(hit.point);
                hitPosT = hit.point;
            }
            dummiesArray[x] = Instantiate(GameObject.Find("PosTestPoint"), new Vector3(hitPosT.x, hitPosT.y, hitPosT.z), Camera.main.transform.rotation);
            dummiesArray[x].GetComponent<MeshRenderer>().material.color = startPoint.GetComponent<MeshRenderer>().material.color;
            dummiesArray[x].name = "PosTestPointEnd";
            SceneManager.MoveGameObjectToScene(dummiesArray[x], SceneManager.GetSceneByName(uiScene));
            endPoint = dummiesArray[x];
            x++;

            if (x > 2){
                Destroy(dummiesArray[x-4]);
                Destroy(dummiesArray[x-3]);
                x = 0;
            }

            lineDist = Vector3.Distance(startPoint.transform.position, endPoint.transform.position);
            counter = 0;
        }

        distance = Vector2.Distance(motor.transform.position, hitPosT);
        
        if (distance <= 0.8f)
        {
            isMoving = false;
        }

        if (stamina == 0){
            GetComponent<NavMeshAgent>().speed = 0;
            myAgent.destination = GameObject.Find("Player_caravan").transform.position;
            isMoving = false;
        } 
        else if (stamina > 0)
        {
            GetComponent<NavMeshAgent>().speed = 10;
        }

        if(counter < lineDist)
        {
            counter += .1f / lineDrawSpeed;
            float x = Mathf.Lerp(0, lineDist, counter);
            Vector3 pointAlongLine = x * Vector3.Normalize(endPoint.transform.position - startPoint.transform.position) + startPoint.transform.position;
            lineRenderer.SetPosition(1, pointAlongLine);
        }

        DebugLines();
    }

    private void DebugLines()
    {
        if (Input.GetKeyDown(KeyCode.F5)) {    
            Debug.Log("\n" + "CarPos" + " " + GameObject.Find("Vehicle_caravan").transform.position + 
                  "\n" + "PlayerPos :" + " " + GameObject.Find("Player_caravan").transform.position + 
                  "\n" + "MotorPos" + " " + motor.transform.position + 
                  "\n" + "HitPos" + " " + hitPosT + 
                  "\n" + "Distance" + " " + distance + "\n");
        }
    }
}