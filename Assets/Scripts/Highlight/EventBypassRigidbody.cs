﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventBypassRigidbody : MonoBehaviour
{
   // Private vars
   private Transform[] children;

   public void OnMouseOver()
   {
      children = gameObject.GetComponentsInChildren<Transform>();
      foreach (var child in children)
      {
         if (child.transform != this.transform)
         {
            child.gameObject.BroadcastMessage("OnMouseOver", options: SendMessageOptions.DontRequireReceiver);
         }
      }
   }

   public void OnMouseExit()
   {
      children = gameObject.GetComponentsInChildren<Transform>();
      foreach (var child in children)
      {
         if (child.transform != this.transform)
         {
            child.gameObject.BroadcastMessage("OnMouseExit", options: SendMessageOptions.DontRequireReceiver);
         }
      }
   }
}
