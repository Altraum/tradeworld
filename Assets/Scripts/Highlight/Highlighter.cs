﻿using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(Collider))]
public class Highlighter : MonoBehaviour
{
    // Private vars
    private MeshRenderer meshRenderer;
    [SerializeField] private Material originalMaterial;
    [SerializeField] private Material highlightedMaterial;

    void Start()
    {
        // Cache a reference to the MeshRenderer
        meshRenderer = GetComponent<MeshRenderer>();

        // Use non-highlighted material by default
        EnableHighlight(false);
    }

    // Toggle betweeen the original and highlighted materials
    public void EnableHighlight(bool onOff)
    {
        if (meshRenderer != null && originalMaterial != null && highlightedMaterial != null)
        {
            meshRenderer.material = onOff ? highlightedMaterial : originalMaterial;
        }
    }

    private void OnMouseOver()
    {
        EnableHighlight(true);
    }

    private void OnMouseExit()
    {
        EnableHighlight(false);
    }
}
