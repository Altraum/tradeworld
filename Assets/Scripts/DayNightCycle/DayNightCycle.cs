﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightCycle : MonoBehaviour
{
    // Private vars
    private float cycle;
    [SerializeField] private float progress;
    [SerializeField] private Component caravanControllerScript;

    // Start is called before the first frame update
    private void Awake()
    {
        cycle = 0;
        progress = 0f;
        this.transform.eulerAngles = new Vector3(cycle,0,0);
    }

    private void Start()
    {
        caravanControllerScript = (Component)GameObject.FindObjectOfType(typeof(CaravanController));
    }
    
    // Update is called once per frame
    private void Update()
    {
        if (caravanControllerScript.GetComponent<CaravanController>().isMoving == true) {
            this.transform.eulerAngles = new Vector3((cycle+progress),0,0);
            progress = progress + 0.3f;
        }
    }
}
