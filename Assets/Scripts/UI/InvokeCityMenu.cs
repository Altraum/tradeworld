﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InvokeCityMenu : MonoBehaviour
{
    // Private vars
    [SerializeField] private GameObject cityMenuButton;
    private GameObject instantiatedButton;
    [SerializeField] private Component selectionManager;
    [SerializeField] private Component caravanPosChecked;
    [SerializeField] private CamController camControllerScript;
    private float distanceToCity;

    // Public vars
    public GameObject[] buttonArray;
    public GameObject screenSpace;
    public int buttonArraySize;
    public int maxDistanceToCity;
    public string uiScene;
    
    private void Start()
    {
        MenuConstructor();
        selectionManager = (Component)GameObject.FindObjectOfType(typeof(SelectionManager));
        caravanPosChecked = (Component)GameObject.FindObjectOfType(typeof(CaravanController));
        camControllerScript = (CamController)FindObjectOfType<CamController>();
    }

    private void Update()
    {
        DisplayMenu();
        DistanceChecker();
    }

    private void DistanceChecker()
    {
        if (selectionManager.GetComponent<SelectionManager>().currentSelection != null && selectionManager.GetComponent<SelectionManager>().hit.transform.gameObject.name.EndsWith("city", StringComparison.OrdinalIgnoreCase))
        {
            distanceToCity = Vector2.Distance(caravanPosChecked.gameObject.transform.position, selectionManager.GetComponent<SelectionManager>().currentSelection.transform.position);
        }
    }

    private void MenuConstructor()
    {
        
        buttonArray = new GameObject[buttonArraySize];

        for (int i = 0; i < buttonArraySize; i++)
        {
            buttonArray[i] = instantiatedButton = Instantiate(cityMenuButton);
            buttonArray[i].GetComponent<RectTransform>().anchorMin = new Vector2 (0,0);
            buttonArray[i].GetComponent<RectTransform>().anchorMax = new Vector2 (0,0);
            buttonArray[i].name = "CityMenuButton " + i;
            SceneManager.MoveGameObjectToScene(buttonArray[i], SceneManager.GetSceneByName(uiScene));
            buttonArray[i].transform.SetParent(screenSpace.transform, false);
        }

        buttonArray[0].transform.position = new Vector2(Screen.width / 20, Screen.height / 10);
        buttonArray[0].GetComponentInChildren<Text>().text = "Enter Town";
        buttonArray[0].GetComponentInChildren<Text>().color = Color.black;
        buttonArray[0].gameObject.SetActive(false);
    }

    private void DisplayMenu()
    {
        if (selectionManager.GetComponent<SelectionManager>().currentSelection != null && selectionManager.GetComponent<SelectionManager>().hit.transform.gameObject.name.EndsWith("city", StringComparison.OrdinalIgnoreCase))
        {
            buttonArray[0].gameObject.SetActive(true);
        } else if (selectionManager.GetComponent<SelectionManager>().currentSelection == null)
        {
            buttonArray[0].gameObject.SetActive(false);
        }
        if (buttonArray[0].gameObject.activeSelf == true && distanceToCity < maxDistanceToCity)
        {
            buttonArray[0].gameObject.GetComponent<Button>().interactable = true;
            buttonArray[0].gameObject.GetComponent<Button>().onClick.AddListener(camControllerScript.TownView);       
        } else if (buttonArray[0].gameObject.activeSelf == true && distanceToCity >= maxDistanceToCity)
        {
            buttonArray[0].gameObject.GetComponent<Button>().interactable = false;
        }
    }
}
