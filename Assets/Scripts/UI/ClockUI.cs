﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClockUI : MonoBehaviour {

    // Private vars
    private Transform clockHourHandTransform;
    private Transform clockMinuteHandTransform;
    private Text timeText;
    private const float REAL_SECONDS_PER_INGAME_DAY = 100f;
    private float day;
    private float dayNormalized;
    private float rotationDegreesPerDay;
    private float hoursPerDay;
    private float minutesPerHour;
    private string hoursString;
    private string minutesString;
    private CaravanController caravanControllerScript;

    private void Awake() {
        caravanControllerScript = GameObject.Find("Player_caravan").GetComponent<CaravanController>();
        clockHourHandTransform = transform.Find("hourHand");
        clockMinuteHandTransform = transform.Find("minuteHand");
        timeText = transform.Find("timeText").GetComponent<Text>();
    }

    private void Update() {
        if (caravanControllerScript.isMoving == true) {
            day += Time.deltaTime / REAL_SECONDS_PER_INGAME_DAY;
        }
        dayNormalized = day % 1f;

        rotationDegreesPerDay = 360f;
        clockHourHandTransform.eulerAngles = new Vector3(0, 0, -dayNormalized * rotationDegreesPerDay);

        hoursPerDay = 24f;
        clockMinuteHandTransform.eulerAngles = new Vector3(0, 0, -dayNormalized * rotationDegreesPerDay * hoursPerDay);

        hoursString = Mathf.Floor(dayNormalized * hoursPerDay).ToString("00");

        minutesPerHour = 60f;
        minutesString = Mathf.Floor(((dayNormalized * hoursPerDay) % 1f) * minutesPerHour).ToString("00");

        timeText.text = hoursString + ":" + minutesString;
    }
}
