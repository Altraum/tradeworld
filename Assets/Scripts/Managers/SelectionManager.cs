﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class SelectionManager : MonoBehaviour
{
    // Public vars
    public int selectableLayerBit;
    public LayerMask selectableLayer;
    public GameObject currentSelection;
    public RaycastHit hit;

    private void Awake()
    {
        selectableLayerBit = 9;
        selectableLayer = 1 << selectableLayerBit;
    }

    private void Update()
    {
        checkSelectedObject();
    }

    public void checkSelectedObject()
    {
        if(Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
         
            if(Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.transform.gameObject.layer == 9)
                {
                    currentSelection = hit.transform.gameObject;
                } else if (hit.transform.gameObject.layer != selectableLayer)
                {
                    currentSelection = null;
                }
            }
        }
    }
}
