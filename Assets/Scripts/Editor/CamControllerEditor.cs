﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CamController))]
public class CustomClassEditor : Editor
{
    private SerializedProperty objFields;

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        objFields = serializedObject.GetIterator();
    
        if (objFields.NextVisible(true))
        {
            GUI.enabled = false;
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_Script"));
            GUI.enabled = true;

            DrawPropertiesExcluding(serializedObject, new string[] { "m_Script", "camSteps" });

            do
            {
                // Just did this to mimic the normal inspector
                EditorGUI.BeginDisabledGroup(objFields.name == "camSteps");
                // If we hit our field, do our special draw
                if (objFields.name == "camSteps") {
                    DrawMyStuff(objFields.Copy());
                }
                EditorGUI.EndDisabledGroup();
                
            }
            // Skip the children (the draw will handle it)
            while (objFields.NextVisible(false));
        }
        
        serializedObject.ApplyModifiedProperties();
    }

    public void DrawMyStuff(SerializedProperty aProperty)
    {
        // Draw but don't include the children
        if (EditorGUILayout.PropertyField(aProperty, false))
        {
            EditorGUI.indentLevel++;

            // Head into our children
            aProperty.NextVisible(true);

            // First child is array size
            EditorGUILayout.PropertyField(aProperty, new GUIContent("Number of Steps"), false);

            // Next is the array itself
            int tIndex = 0;
            while (aProperty.NextVisible(false) &&
                   !SerializedProperty.EqualContents(aProperty, aProperty.GetEndProperty()))
            {
                if (tIndex == 0){
                    EditorGUILayout.PropertyField(aProperty, new GUIContent("Low_Step Camera"), true);
                } else if (tIndex == 1){
                    EditorGUILayout.PropertyField(aProperty, new GUIContent("Middle_Step Camera"), true);
                } else if (tIndex == 2){
                    EditorGUILayout.PropertyField(aProperty, new GUIContent("Hight_Step Camera"), true);
                }
                
                tIndex++;
            }

            EditorGUI.indentLevel--;
        }
    }
}