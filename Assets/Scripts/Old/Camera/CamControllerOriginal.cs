﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamControllerOriginal : MonoBehaviour
{
    // Public vars
    public float camNormalSpeed;
    public float camMoveSpeed;
    public float camMoveTime;
    public float camRotationSpeed;
    public float switchSpeed;
    public float screenBorderThickness;
    public Camera Low_camera;
    public Camera Middle_camera;
    public Camera High_camera;

    // Private vars
    private Camera[] cameras;
    private int currentCameraIndex;
    private int mCurrentIndex;
    private Vector3[] Positions;
    private Vector3 newPos;
    private Vector3 terrainSize;
    private Vector3 terrainCenter;

    // Start is called before the first frame update
    private void Start() {
        Positions = new Vector3[3];
        mCurrentIndex = 1;
        cameras = new Camera[3];
        currentCameraIndex = 1;
        cameras[0] = Low_camera;
        cameras[1] = Middle_camera;
        cameras[2] = High_camera;
        cameras[0].enabled = false;
        cameras[2].enabled = false;
        // Place camera on caravan when scene starts
        newPos = GameObject.Find("Vehicle_caravan/Caravan").GetComponent<MeshRenderer>().bounds.center;
        newPos.y = 0;
        terrainSize = GameObject.Find("Terrain").GetComponent<Terrain>().terrainData.size;
        terrainCenter = new Vector3(GameObject.Find("Terrain").GetComponent<TerrainCollider>().bounds.center.x,GameObject.Find("Terrain").GetComponent<TerrainCollider>().bounds.center.y,GameObject.Find("Terrain").GetComponent<TerrainCollider>().bounds.center.z);
        // Details debug commands in console at start
        Debug.Log("\n Debug tools : \n Press F1 to display cameras position \n Press F2 to display terrain dimensions \n Press F3 to display terrain centers position \n Press F4 to display terrain limits position");
    }
    
    // Update is called once per frame
    private void Update() {
        InputHandler();
        DebugHandler();
    }
  
    // Input behaviors
    private void InputHandler() {
        // Changes camera speed if LeftShift is held
        if (Input.GetKey(KeyCode.LeftShift)) {
            camMoveSpeed = camNormalSpeed * 2.5f;
        } else {
            camMoveSpeed = camNormalSpeed;
        }
        // Changes camera speed if LeftShift is held and mouse is on the edge of the screen
        if (Input.GetKey(KeyCode.LeftShift) && (Input.mousePosition.y >= Screen.height - screenBorderThickness || 
            Input.mousePosition.y <= screenBorderThickness || Input.mousePosition.x >= Screen.width - screenBorderThickness || Input.mousePosition.x <= screenBorderThickness)) {
            camMoveSpeed = camNormalSpeed * 3.5f;
        }
        // Changes camera speed if ZQSD is held and mouse is on the edge of the screen
        if ((Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.Q)) && (Input.mousePosition.y >= Screen.height - screenBorderThickness || Input.mousePosition.y <= screenBorderThickness || Input.mousePosition.x >= Screen.width - screenBorderThickness || Input.mousePosition.x <= screenBorderThickness)) {
            camMoveSpeed = camNormalSpeed * 3.5f;
        }
        // Changes camera speed if ZQSD and LeftShift are held and mouse is on the edge of the screen
        if (Input.GetKey(KeyCode.LeftShift) && (Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.Q)) && (Input.mousePosition.y >= Screen.height - screenBorderThickness || Input.mousePosition.y <= screenBorderThickness || Input.mousePosition.x >= Screen.width - screenBorderThickness || Input.mousePosition.x <= screenBorderThickness)) {
            camMoveSpeed = camNormalSpeed * 5f;
        }
        // Rotate camera with A/E/MouseWheel
        if (Input.GetKey(KeyCode.A)) {
            transform.Rotate(0f,camRotationSpeed,0f);
        }
        if (Input.GetKey(KeyCode.E)) {
            transform.Rotate(0f,-camRotationSpeed,0f);
        }
        if (Input.GetKey(KeyCode.Mouse2) && (Input.mousePosition.x > Screen.width/2)) {
            transform.Rotate(0f,camRotationSpeed,0f);
        }
        if (Input.GetKey(KeyCode.Mouse2) && (Input.mousePosition.x < Screen.width/2)) {
            transform.Rotate(0f,-camRotationSpeed,0f); 
        }
        // ZQSD and Arrows movements
        if (Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.UpArrow) || Input.mousePosition.y >= Screen.height - screenBorderThickness) {
            newPos += (transform.forward * camMoveSpeed);
        } 
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow) || Input.mousePosition.y <= screenBorderThickness) {
            newPos += (transform.forward * -camMoveSpeed);
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow) || Input.mousePosition.x >= Screen.width - screenBorderThickness) {
            newPos += (transform.right * camMoveSpeed);
        }
        if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow) || Input.mousePosition.x <= screenBorderThickness) {
            newPos += (transform.right * -camMoveSpeed);
        }
        // Switch between cameras if C is pressed down
        if (Input.GetKeyDown(KeyCode.C)) {
            cameras[currentCameraIndex].enabled = false;
            currentCameraIndex++;
            currentCameraIndex %= 3;
            cameras[currentCameraIndex].enabled = true;
        }
        Positions[0] = new Vector3(transform.position.x, transform.position.y + 20, transform.position.z - 15);
        Positions[1] = new Vector3(transform.position.x, transform.position.y + 90, transform.position.z - 75);
        Positions[2] = new Vector3(transform.position.x, transform.position.y + 190, transform.position.z - 150);
        if (Input.GetKeyDown(KeyCode.X)) {
            mCurrentIndex++;
            mCurrentIndex %= 3;
        }
        // Interpolation to smooth camera movements (forward, backward, sides)
        transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime * camMoveTime);
        // Interpoaltion to smooth camera transitions (low, middle, high)
        this.gameObject.transform.GetChild(1).transform.position = Vector3.Lerp(this.gameObject.transform.GetChild(1).transform.position, Positions[mCurrentIndex], switchSpeed * Time.deltaTime);
        // Limit camera movements to the edges of the terrain
        newPos.x = Mathf.Clamp(newPos.x, (terrainCenter.x + (-terrainSize.x/2)), ((terrainCenter.x + (-terrainSize.x/2) + terrainSize.x)));
        newPos.z = Mathf.Clamp(newPos.z, (terrainCenter.z + (-terrainSize.z/2)), ((terrainCenter.z + (-terrainSize.z/2) + terrainSize.z)));
    }

    // Debug method
    private void DebugHandler() {
        // Cameras and rig positions debug when pressing F1
        if (Input.GetKeyDown(KeyCode.F1)) {
            Debug.Log("\n" + this.gameObject.transform.GetChild(0) + " " + "PosX" + " " + Mathf.Round(this.gameObject.transform.GetChild(0).gameObject.transform.position.x) + " " + "PosY" + " " + Mathf.Round(this.gameObject.transform.GetChild(0).gameObject.transform.position.y) + " " + "PosZ" + " " + Mathf.Round(this.gameObject.transform.GetChild(0).gameObject.transform.position.z) + 
                      "\n" + this.gameObject.transform.GetChild(1) + " " + "PosX" + " " + Mathf.Round(this.gameObject.transform.GetChild(1).gameObject.transform.position.x) + " " + "PosY" + " " + Mathf.Round(this.gameObject.transform.GetChild(1).gameObject.transform.position.y) + " " + "PosZ" + " " + Mathf.Round(this.gameObject.transform.GetChild(1).gameObject.transform.position.z) + 
                      "\n" + this.gameObject.transform.GetChild(2) + " " + "PosX" + " " + Mathf.Round(this.gameObject.transform.GetChild(2).gameObject.transform.position.x) + " " + "PosY" + " " + Mathf.Round(this.gameObject.transform.GetChild(2).gameObject.transform.position.y) + " " + "PosZ" + " " + Mathf.Round(this.gameObject.transform.GetChild(2).gameObject.transform.position.z) +
                      "\n" + this.gameObject.name + " " + "PosX" + " " + Mathf.Round(this.gameObject.transform.position.x) + " " + "PosY" + " " + Mathf.Round(this.gameObject.transform.position.y) + " " + "PosZ" + " " + Mathf.Round(this.gameObject.transform.position.z));
        }
        // Terrain dimensions debug when pressing F2
        if (Input.GetKeyDown(KeyCode.F2)) {    
            Debug.Log("\n" + ("Terrain X Size : " + terrainSize.x) + 
                      "\n" + ("Terrain Z Size : " + terrainSize.z));
        }
        // Terrain centers position debug when pressing F3
        if (Input.GetKeyDown(KeyCode.F3)) {
            Debug.Log("\n" + "Terrain Center X" + " " + terrainCenter.x +
                      "\n" + "Terrain Center Z" + " " + terrainCenter.z);
        }
        // Terrain lower and upper limits debug when pressing F4
        if (Input.GetKeyDown(KeyCode.F4)) {
        Debug.Log("\n" + "Lower X Limit" + (terrainCenter.x + (-terrainSize.x/2)) + " " + "Upper X Limit" + " " + ((terrainCenter.x + (-terrainSize.x/2) + terrainSize.x)) + 
                  "\n" + "Lower Z Limit" + (terrainCenter.z + (-terrainSize.z/2)) + " " + "Upper Z Limit" + " " + ((terrainCenter.z + (-terrainSize.z/2) + terrainSize.z)));
        }
    }
}